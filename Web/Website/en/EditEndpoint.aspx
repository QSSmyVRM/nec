<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" Inherits="ns_EditEndpoint.EditEndpoint" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!--Window Dressing-->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Create/Edit Endpoint</title>
    <%--ZD 100664 Start--%>
    <script src="script/CallMonitorJquery/jquery.1.4.2.js" type="text/javascript"></script> 
    <script type="text/javascript">
        $(document).ready(function() {
            $('.treeNode').click(function() {
                var target = $(this).attr('tag');
                if ($('#' + target).is(":visible")) {
                    $('#' + target).hide("slow");
                    $(this).attr('src', 'image/loc/nolines_plus.gif');
                } else {
                    $('#' + target).show("slow");

                    $(this).attr('src', 'image/loc/nolines_minus.gif');
                }

            });
        });
    </script>
    <%--ZD 100664 End--%>
<script language="javascript">
  //ZD 100604 start
  var img = new Image();
  img.src = "../en/image/wait1.gif";
  //ZD 100604 End
    function fnTextFocus(xid,par) {
    
      var custId =xid.split("_");
          
    document.getElementById(custId[0]+"_"+custId[1]+"_"+"lbltxtCompare").value = true;
    // ZD 100263 Starts
    var obj1 =  document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword");
    var obj2 =  document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword2");
    
    document.getElementById(custId[0]+"_"+custId[1]+"_"+"hdnPass").value = obj1.value;
    if (par == 1)
        document.getElementById(custId[0]+"_"+custId[1]+"_"+"lbltxtProfilePassword").value = true;
    else
        document.getElementById(custId[0]+"_"+custId[1]+"_"+"lbltxtProfilePassword2").value = true; 
         if (obj1.value == "" && obj2.value == "") {
         document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword").style.backgroundImage = "";
         document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword2").style.backgroundImage = "";
        document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword").value = "";
         document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword2").value = "";
    }
    return false;
    // ZD 100263 Ends 
    if (par == 1) {
    
        if(document.getElementById(custId[0]+"_"+custId[1]+"_"+"lbltxtProfilePassword2") != null)
        {
            if(document.getElementById(custId[0]+"_"+custId[1]+"_"+"lbltxtProfilePassword2").value == "true")
            { 
                document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword").value = "";                
                
            }else
            {
                document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword").value = "";
                document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword2").value = "";
            }
        }
        else
        {
            document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword").value = "";                
            document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword2").value = "";                
        }
    }
       else{
       if (obj1.value == "" && obj2.value == "") {
         document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword").style.backgroundImage = "";
         document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword2").style.backgroundImage = "";
        document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword").value = "";
         document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword2").value = "";
    }
    return false;
           if(document.getElementById(custId[0]+"_"+custId[1]+"_"+"lbltxtProfilePassword") != null)
           {
            if(document.getElementById(custId[0]+"_"+custId[1]+"_"+"lbltxtProfilePassword").value == "true")
            { 
                
                document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword2").value = "";                
            }
            else{
            document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword").value = "";                
                document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword2").value = "";
                }
           
           }
           else{
                document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword").value = "";                
                document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtProfilePassword2").value = "";
                
                }
        }
        
         if(document.getElementById(custId[0]+"_"+custId[1]+"_"+"cmpPass1")!= null)
        {
            ValidatorEnable(document.getElementById(custId[0]+"_"+custId[1]+"_"+"cmpPass1"), false);
            ValidatorEnable(document.getElementById(custId[0]+"_"+custId[1]+"_"+"cmpPass1"), true);
        }
         if(document.getElementById(custId[0]+"_"+custId[1]+"_"+"cmpPass2")!= null)
        {
            ValidatorEnable(document.getElementById(custId[0]+"_"+custId[1]+"_"+"cmpPass2"), false);
            ValidatorEnable(document.getElementById(custId[0]+"_"+custId[1]+"_"+"cmpPass2"), true);
        }
}
function ValidateSelection(obj)
{

    var lstBridges = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_lstBridges"); //FB 2093
    var lstProtocol = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_lstVideoProtocol");
    var lstConnectionType = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_lstConnectionType");
    var lstMCUAddressType = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_lstMCUAddressType");
    var lstAddressType = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_lstAddressType");
    var reqBridges = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_reqBridges");
    var regAddress = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_regAddress");
    var reqPrefDial = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_reqPrefDial");    //FB 1394
    var reqManufac = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_reqManufacturerType");//ZD 100736
    //var reqResolution = document.getElementById(obj.id.substring(0,obj.id.lastIndexOf("_")) + "_reqResolution");//ZD 100040
   
    
    if("<%=Session["isAssignedMCU"]%>" == "1" || ("<%=Session["isAssignedMCU"]%>" == "0" && lstBridges.value != "-1")) //FB 2093
        ValidatorEnable(reqPrefDial, true);    //FB 1394
    
    if (obj == lstAddressType)
    {
        EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), true);
        //ValidatorEnable(reqBridges, false);   //FB 1394
        //commented for ZD 103595- Operations - Remove MPI Functionality and Verbiage START
//        if (lstAddressType.value == 5)
//        {
//            EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), false);
//            lstProtocol.selectedIndex = 4;
//            lstConnectionType.selectedIndex = 3;
//            ValidatorEnable(reqPrefDial, false);    //FB 1394
//            lstMCUAddressType.selectedIndex = 5;
//            //ValidatorEnable(reqBridges, true);    //FB 1394
//        }
 //commented for ZD 103595- Operations - Remove MPI Functionality and Verbiage - END
        if (lstAddressType.value == 4)
        {
            lstProtocol.selectedIndex = 2;
            lstConnectionType.selectedIndex = 2;
            ValidatorEnable(reqPrefDial, false);    //FB 1394
            lstMCUAddressType.selectedIndex = 4;
        }
        else
        {
            if (lstProtocol.value == 2) //Removed MPI protocol for ZD 103595- Operations - Remove MPI Functionality and Verbiage START
                lstProtocol.selectedIndex = 1;
            if (lstConnectionType.value == 3)
            {
                lstConnectionType.selectedIndex = 1;
                ValidatorEnable(reqPrefDial, false);    //FB 1394
            }
            if (lstMCUAddressType.value == 4)//Removed MPI MCU adress type for ZD 103595- Operations - Remove MPI Functionality and Verbiage START
                lstMCUAddressType.selectedIndex = 1;
        }
    }
    if (obj == lstMCUAddressType)
    {
        EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), true);
        //ValidatorEnable(reqBridges, false);   //FB 1394
        //Removed MPI MCU adress type for ZD 103595- Operations - Remove MPI Functionality and Verbiage START
//        if (lstMCUAddressType.value == 5)
//        {
//            lstProtocol.selectedIndex = 4;
//            lstConnectionType.selectedIndex = 3;
//            ValidatorEnable(reqPrefDial, false);    //FB 1394
//            lstAddressType.selectedIndex = 5;
//            //ValidatorEnable(reqBridges, true);    //FB 1394
//            EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), false);
//        }
        if (lstMCUAddressType.value == 4)
        {
            lstProtocol.selectedIndex = 2;
            lstConnectionType.selectedIndex = 2;
            ValidatorEnable(reqPrefDial, false);    //FB 1394
            lstAddressType.selectedIndex = 4;
        }
        else
        {
            if (lstProtocol.value == 2)//Removed MPI Protocol for ZD 103595- Operations - Remove MPI Functionality and Verbiage START
                lstProtocol.selectedIndex = 1;
            if (lstConnectionType.value == 3)
            {
                lstConnectionType.selectedIndex = 1;
                ValidatorEnable(reqPrefDial, false);    //FB 1394
            }
            if (lstAddressType.value == 4)//Removed MPI adress type for ZD 103595- Operations - Remove MPI Functionality and Verbiage START
                lstAddressType.selectedIndex = 1;
        }
    }
    if (obj == lstConnectionType)
    {
        EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), true);
        //ValidatorEnable(reqBridges, false);   //FB 1394
        if (lstConnectionType.value == 3) //MPI-Direct
        {
            lstProtocol.selectedIndex = 4;
            lstAddressType.selectedIndex = 5;
            lstMCUAddressType.selectedIndex = 5;
            //ValidatorEnable(reqBridges, true);    //FB 1394
            EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), false);
        }
        else //If not MPI-Direct
        {
            if (lstProtocol.value == 4)
                lstProtocol.selectedIndex = 1;
            if (lstAddressType.value == 5)
                lstAddressType.selectedIndex = 1;
            if (lstMCUAddressType.value == 5)
                lstMCUAddressType.selectedIndex = 1;
        }
    }
    if (obj == lstProtocol)
    {
        EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), true);
        //ValidatorEnable(reqBridges, false);   //FB 1394
//        if (lstProtocol.value == 4)//Removed MPI protocol for ZD 103595- Operations - Remove MPI Functionality and Verbiage START
//        {
//            lstMCUAddressType.selectedIndex = 5;
//            lstConnectionType.selectedIndex = 3;
//            ValidatorEnable(reqPrefDial, false);    //FB 1394
//            lstAddressType.selectedIndex = 5;
//            //ValidatorEnable(reqBridges, true);    //FB 1394
//            EnableDefaults(obj.id.substring(0,obj.id.lastIndexOf("_")), false);
//        }
        if (lstProtocol.value == 2)
        {
            lstMCUAddressType.selectedIndex = 4;
            if (lstConnectionType.value == 3)
            {
                lstConnectionType.selectedIndex = 1;
                ValidatorEnable(reqPrefDial, false);    //FB 1394
            }
            lstAddressType.selectedIndex = 4;
        }
        else
        {
            if (lstMCUAddressType.value == 4)//Removed MPI MCU adress type for ZD 103595- Operations - Remove MPI Functionality and Verbiage START
                lstMCUAddressType.selectedIndex = 1;
            if (lstConnectionType.value == 3)
            {
                lstConnectionType.selectedIndex = 1;
                ValidatorEnable(reqPrefDial, false);    //FB 1394
            }
            if (lstAddressType.value == 4)//Removed MPI MCU adress type for ZD 103595- Operations - Remove MPI Functionality and Verbiage START
                lstAddressType.selectedIndex = 1;
        }
    }
}

function EnableDefaults(varName, flag)
{
    var elements = document.getElementsByTagName('input'); 
    var chkDefault = varName + "_rdDefault";
    var chkDelete = varName + "_chkDelete";
    
    if (flag == false)
        document.getElementById("IsMarkedDeleted").value = "1";
    else
        document.getElementById("IsMarkedDeleted").value = "0"; 
   
    for (i=0;i<elements.length;i++)
    {
        if ( (elements.item(i).type == "checkbox") && (elements.item(i).id.indexOf("Default") >= 0) )  //ZD 100815
        {
            if(elements.item(i).id != chkDefault) // all rows but the current datagrid row
            { 
                if (flag == false) //if MPI
                    elements.item(i).checked = flag; 
                elements.item(i).disabled = !flag; 
            }
            else // in current row
            {
                if (flag == false) //if MPI
                    elements.item(i).checked= !flag; 
                else
                    elements.item(i).disabled = !flag; 
            }
        }
        if ( (elements.item(i).type == "checkbox") && (elements.item(i).id.indexOf("Delete") >= 0) ) 
        {                
            if(elements.item(i).id != chkDelete) // all rows but the current datagrid row
            {
                elements.item(i).disabled = !flag; 
                elements.item(i).checked = !flag;
            }
            else
            {
                if (flag == false) // if MPI then disable the current row delete check box 
                {
                    elements.item(i).checked= flag; 
                    elements.item(i).disabled = !flag;
                }
                else // if not MPI then enable the delete checkbox
                     elements.item(i).disabled = !flag;
            }
            if (elements.item(i).checked)
                    ValidatorEnable(document.getElementById(varName + "_regAddress"), false);
            else
                    ValidatorEnable(document.getElementById(varName + "_regAddress"), true);
        }
    }
}

function IsMarkedForDeletion()
{
    if (typeof(Page_ClientValidate) == 'function') 
        if (!Page_ClientValidate())
            return false;
    if (document.getElementById("IsMarkedDeleted").value == "1")
    {
        if (confirm(EndpointMPI))
            return true;
        else return false;
    }
    else return true;
}
//ZD 100176 start
		function DataLoading(val) {
		    if (val == "1")
		        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
		    else
		        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
	}
    //ZD 100176  End                 


function SelectOneDefault(obj)
{
    var selprofiletype = obj.id.substring(0,obj.id.indexOf("rdDefault")) + "lstProfileType"; //ZD 100815
    if (obj.tagName == "INPUT" && obj.type == "checkbox" && obj.checked)  
    {
        var elements = document.getElementsByTagName('input'); 
        var chkDefault = obj.id.substring(0,obj.id.indexOf("rdDefault")) + "chkDelete";
        var objDef = document.getElementById(chkDefault);
        //ZD 100815 start
        var chkDefaultProfType =obj.id.substring(0,obj.id.indexOf("rdDefault")) + "rdDefault";
        var objprofileType = document.getElementById(chkDefaultProfType);
        if(objprofileType.checked)
        {
        document.getElementById(selprofiletype).disabled =true;
         if  (document.getElementById("dgProfiles") != null) {  
              if (document.getElementById("dgProfiles_ctl02_lstProfileType") != null)
                  var dgrow = document.getElementById("dgProfiles").rows.length;
                  var ProfileBothcount =0  , ProfileMCUcount =0 ,ProfileP2Pcount =0;                
                for (var i = 2; i < dgrow + 1 ; i++) {
                    if (i < 9)
                        i = "0" + i;
                    if (document.getElementById("dgProfiles_ctl" + i + "_lstProfileType") != null)
                       if((document.getElementById("dgProfiles_ctl" + i + "_lstProfileType").selectedIndex == 0) &&  (document.getElementById("dgProfiles_ctl" + i + "_rdDefault").checked ==true ))
                         ProfileBothcount = ProfileBothcount +1; 
                       else if((document.getElementById("dgProfiles_ctl" + i + "_lstProfileType").selectedIndex == 1) && (document.getElementById("dgProfiles_ctl" + i + "_rdDefault").checked ==true ))
                         ProfileMCUcount = ProfileMCUcount +1; 
                         else if((document.getElementById("dgProfiles_ctl" + i + "_lstProfileType").selectedIndex == 2) && (document.getElementById("dgProfiles_ctl" + i + "_rdDefault").checked ==true ))
                           ProfileP2Pcount = ProfileP2Pcount + 1;
                }
            }
            var count=0,ItemId,ProfileTypeVal,rdDefaultVal; //ZD 101490
                for (i=0;i<elements.length;i++)
                if ( (elements.item(i).type == "checkbox") && (elements.item(i).id.indexOf("Default") >= 0) ) 
                {
                    if(elements.item(i).checked ==true)
                    { 
                        if(ProfileBothcount > 1 || ProfileMCUcount > 1 || ProfileP2Pcount > 1 || (ProfileBothcount ==1 &&  (ProfileMCUcount == 1 || ProfileP2Pcount ==1))){
                        if(elements.item(i).id!=obj.id) //ZD 101490 Start
                            {   
                                ItemId =elements.item(i).id.split('_')[1];                        
                                ProfileTypeVal = document.getElementById("dgProfiles_" + ItemId  + "_lstProfileType").selectedIndex;
                                rdDefaultVal = document.getElementById("dgProfiles_" + ItemId + "_rdDefault");

                                if((ProfileTypeVal == document.getElementById(selprofiletype).selectedIndex || ProfileTypeVal == 0) && (rdDefaultVal.checked ==true ))
                                    rdDefaultVal.checked = false;

                                if((document.getElementById(selprofiletype).selectedIndex == 0))
                                    elements.item(i).checked= false;
                            } //ZD 101490 End
                            document.getElementById(selprofiletype).disabled =false;
                        }
                    }
                } 
            
        }
        //ZD 100815 end
//        alert(chkDefault);
        if(objDef.checked)
        {
//            obj.checked = false;
            alert(ConfDelProf);
            objDef.checked = false;
        }
    }
    else
      document.getElementById(selprofiletype).disabled =false;
	//ZD 100815 end
}  
//FB 2595 Start
function fnSecuredDetails(obj)
{
   var custId = obj.id.split("_");
  
   var tdNetworkURL = document.getElementById(custId[0]+"_"+custId[1]+"_"+"lblNetworkURL"); //"tdNetworkURL"
   var tdTxtNetworkURL = document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtNetworkURL");
   var tdSecuredPort = document.getElementById(custId[0]+"_"+custId[1]+"_"+"lblSecuredPort");
   var tdTxtSecuredPort = document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtSecuredport");
   
   if(obj.checked)
   {
        tdNetworkURL.style.visibility = 'visible';
        tdTxtNetworkURL.style.visibility = 'visible';
        tdSecuredPort.style.visibility = 'visible';
        tdTxtSecuredPort.style.visibility = 'visible';
   }
   else
   {
        tdNetworkURL.style.visibility = 'hidden';
        tdTxtNetworkURL.style.visibility = 'hidden';
        tdSecuredPort.style.visibility = 'hidden';
        tdTxtSecuredPort.style.visibility = 'hidden';
   }
}
//FB 2595 End

//ZD 100132 START
function fnOpengatekeeper(obj)
{
   var custId = obj.id.split("_");
  
   var tdGateKeeeperAddress = document.getElementById(custId[0]+"_"+custId[1]+"_"+"LblGateKeeeperAddress");
   var trTxtSecuredPort = document.getElementById(custId[0]+"_"+custId[1]+"_"+"txtGateKeeeperAddress");
   
   if(obj.checked)
   {
        trTxtSecuredPort.style.visibility = 'visible';
        tdGateKeeeperAddress.style.visibility = 'visible';
           }
   else
   {
       trTxtSecuredPort.style.visibility = 'hidden';
       tdGateKeeeperAddress.style.visibility = 'hidden';
          }
}
//ZEN 100132 END

//FB 2044 - Starts
function CheckDefault(obj)
{
    var rdDefault = obj.id.substring(0,obj.id.indexOf("chkDelete")) + "rdDefault";
    var objDef = document.getElementById(rdDefault);
   
    if (obj.checked)
    {
        var objDef1 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqName");
        var objDef2 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "regProfileName"); 
        var objDef3 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "regnumPassword1"); 
        var objDef4 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "cmpPass1"); 
        var objDef5 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "regnumPassword2"); 
        var objDef6 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "cmpPass2"); 
        var objDef7 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "regAddress"); 
        var objDef8 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "regMPI"); 
        var objDef9 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqAddressType"); 
        var objDef10 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqVideoEquipment"); 
        var objDef11 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqAddress");
        var objDef12 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqLineRate"); 
        var objDef13 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqBridges"); 
        var objDef14 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqPrefDial"); 
        var objDef15 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "RegMCUAddress");
        var objDef16 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "RegURL"); 
        var objDef17 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "RegExchangeID"); 
        var objDef18 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "RegApiport");
        var objDef19 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "regRearSecCamAdd");
        var objDef20 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqManufacturerType");//ZD 100736
        var objDef21 = document.getElementById(obj.id.substring(0,obj.id.indexOf("chkDelete")) + "reqEptResolution");//ZD 100040


          
        
        if(objDef.checked)
        {
            obj.checked = false;
            alert(DefaultProfdel);
        }
        
        ValidatorEnable(objDef1, false);
        objDef1.style.display = 'none';

        ValidatorEnable(objDef2, false);
        objDef2.style.display = 'none';

        ValidatorEnable(objDef3, false);
        objDef3.style.display = 'none';

        ValidatorEnable(objDef4, false);
        objDef4.style.display = 'none';

        ValidatorEnable(objDef5, false);
        objDef5.style.display = 'none';

        ValidatorEnable(objDef6, false);
        objDef6.style.display = 'none';

        ValidatorEnable(objDef7, false);
        objDef7.style.display = 'none';

        ValidatorEnable(objDef8, false);
        objDef8.style.display = 'none';

        ValidatorEnable(objDef9, false);
        objDef9.style.display = 'none';

        ValidatorEnable(objDef10, false);
        objDef10.style.display = 'none';

        ValidatorEnable(objDef11, false);
        objDef11.style.display = 'none';

        ValidatorEnable(objDef12, false);
        objDef12.style.display = 'none';

        ValidatorEnable(objDef13, false);
        objDef13.style.display = 'none';

        ValidatorEnable(objDef14, false);
        objDef14.style.display = 'none';

        ValidatorEnable(objDef15, false);
        objDef15.style.display = 'none';

        ValidatorEnable(objDef16, false);
        objDef16.style.display = 'none';

        ValidatorEnable(objDef17, false);
        objDef17.style.display = 'none';

        ValidatorEnable(objDef18, false);
        objDef18.style.display = 'none';

        ValidatorEnable(objDef19, false);
        objDef19.style.display = 'none';

        ValidatorEnable(objDef21, false); //ZD 100040
        objDef12.style.display = 'none';

    }
}
//FB 2044 - End

</script>
</head>
<body>
    <form id="frmSearchConference" runat="server" method="post" onsubmit="DataLoading(1)" autocomplete="off"><%--ZD 100176--%>  <%--ZD 101190--%>
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
        <center><table border="0" width="100%" cellpadding="2" cellspacing="2">
            <tr>
                <td align="center">
                    <h3><asp:Label ID="lblHeader" runat="server" Text="<%$ Resources:WebResources, EditEndpoint_lblHeader%>"></asp:Label></h3><br />
                    <asp:Label ID="errLabel" runat="server" CssClass="lblError"></asp:Label>
                   <div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
                        <img border='0' src='image/wait1.gif' alt='Loading..' />
                   </div><%--ZD 100678--%>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table>
                    <%--ZD 100664 - Start --%>
                    <tr id="trHistory" runat="server">
                    <td colspan="2">
                        <table width="100%" border="0">
                            <tr>
                                <td valign="top" align="left" style="width: 2px">
                                <span class="blackblodtext" style="vertical-align:top"><asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, History%>" runat="server"></asp:Literal></span>
                                    <a href="#" onmouseup="if(event.keyCode == 13){this.childNodes[0].click();return false;}" onkeydown="if(event.keyCode == 13){document.getElementById('imgHistory').click();return false;}" >
                                    <img id="imgHistory" src="image/loc/nolines_plus.gif" class="treeNode" style="border:none" alt="Expand/Collapse" tag="tblHistory" /></a>
                                </td>
                            </tr>
                        </table>
                    </td>
                    </tr>
                    <tr>
                    <td colspan="2"> 
                      <div id="tblHistory" style="display:none; width:600px; float:left">
                        <table cellpadding="4" cellspacing="0" border="0" style="border-color:Gray; border-radius:15px; background-color:#ECE9D8"  width="75%" class="tableBody" align="center" >
                            <tr>
                                <td class="subtitleblueblodtext" align="center">
                                    <asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, Details%>" runat="server"></asp:Literal>
                                </td>            
                            </tr>
                            <tr align="center">
                                <td align="left">
                                    <asp:DataGrid ID="dgChangedHistory" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="true" 
                                        BorderStyle="solid" BorderWidth="0" ShowFooter="False"  
                                        Width="100%" Visible="true" style="border-collapse:separate" >
                                        <SelectedItemStyle  CssClass="tableBody"/>
                                        <AlternatingItemStyle CssClass="tableBody" />
                                        <ItemStyle CssClass="tableBody"  />                        
                                        <FooterStyle CssClass="tableBody"/>
                                        <HeaderStyle CssClass="tableHeader" />
                                        <Columns>
                                            <asp:BoundColumn DataField="ModifiedUserId"  HeaderStyle-Width="0%" Visible="false" ItemStyle-BackColor="#ECE9D8" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ModifiedDateTime" HeaderText="<%$ Resources:WebResources, Date%>" ItemStyle-BackColor="#ECE9D8" ></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ModifiedUserName" HeaderText="<%$ Resources:WebResources, User%>"  ItemStyle-BackColor="#ECE9D8" ></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Description" HeaderText="<%$ Resources:WebResources, Description%>"  ItemStyle-BackColor="#ECE9D8" ></asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                    </td>
                                </tr>
                            </table>
                            <br />
                      </div>
                      <br />
                    </td>
                    </tr> 
                    <%--ZD 100664 - End --%>
                    <tr>
                    
                        <td>
                            <SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, EditEndpoint_EndpointName%>" runat="server"></asp:Literal><span style="color:Red">*</span></SPAN>
                        </td>
                        <td>
                            <asp:TextBox ID="txtEndpointName" runat="server" CssClass="altText" Text="" MaxLength="20" Width="500" ></asp:TextBox><%--FB 2523--%><%--FB 2995--%>
                            <asp:RequiredFieldValidator ID="reqEndpointName" SetFocusOnError="true" runat="server" ControlToValidate="txtEndpointName" ErrorMessage="<%$ Resources:WebResources, Required%>"  Display="dynamic" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                                            <%-- Code Added for FB 1640--%>                                                
                            <asp:RegularExpressionValidator ID="regEndpointName" SetFocusOnError="true" ControlToValidate="txtEndpointName" Display="dynamic" runat="server" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters13%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@$%&'~]*$"></asp:RegularExpressionValidator> <%--FB 1888--%>
                            <asp:TextBox CssClass="altText"  ID="txtEndpointID" runat="server" Visible="false"></asp:TextBox>
                        </td>
                    </tr>
                    </table>
                </td>
            </tr>
                <asp:DataGrid runat="server" OnItemDataBound="InitializeLists" OnItemCreated="InitializeLists" ID="dgProfiles" AutoGenerateColumns="false"
                  CellSpacing="0" CellPadding="4" GridLines="None" BorderColor="blue" BorderStyle="solid" BorderWidth="1"  Width="100%" style="border-collapse:separate"><%-- Edited for FF--%>
               <%--Window Dressing - Start--%>
                <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                <EditItemStyle CssClass="tableBody" />
                <AlternatingItemStyle CssClass="tableBody" />
                <ItemStyle CssClass="tableBody" />
                <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                <Columns>
                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, ProfileNo%>" HeaderStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="center" VerticalAlign="Top" />
                        <HeaderStyle CssClass="tableHeader" />
                        <ItemTemplate>
                            <asp:Label ID="lblProfileCount" Text="" runat="server" Font-Bold="true"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Profiles%>" HeaderStyle-HorizontalAlign="Left">
                    <HeaderStyle CssClass="tableHeader" HorizontalAlign="center" Height="25" />
                        <ItemTemplate>
                        <table width="100%">
                            <tr>
                                <td align="left" class="tableBody" valign="top">
                                    <asp:Literal Text="<%$ Resources:WebResources, EditEndpoint_ProfileName%>" runat="server"></asp:Literal><span style="color:Red">*</span></td>
                                <td align="left" valign="top">
                                    <asp:TextBox ID="txtProfileID" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProfileID") %>'></asp:TextBox>
                                    <asp:TextBox CssClass="altText"  ID="txtProfileName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProfileName") %>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqName" ControlToValidate="txtProfileName" runat="server" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="Dynamic" ></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regProfileName" ControlToValidate="txtProfileName" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters19%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`\[\]{}\x22;=^:@$%&'~]*$"></asp:RegularExpressionValidator>  <%--FB 2954--%>
                                </td>
                                <%--ZD 100814--%>
                                <td align="left" class="tableBody" valign="top">
                                    <asp:Label ID="lblUserName" Text="<%$ Resources:WebResources, UserName%>" runat="server"></asp:Label></td>
                                <td align="left" valign="top">
                                    <asp:TextBox CssClass="altText"  ID="txtUserName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.UserName") %>'></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="regUsrNme" ControlToValidate="txtUserName" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters19%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`\[\]{}\x22;=^:@$%&'~]*$"></asp:RegularExpressionValidator>  
                                </td>
                                <td >
                                    <table width="100%" cellspacing="0">
                                        <tr valign="top">
                                            <td align="left" class="tableBody" valign="top" >
                                                <asp:Label ID="lblPassword" Text="<%$ Resources:WebResources, EditEndpoint_Password%>" runat="server"></asp:Label>
                                            </td>
                                        </tr> <%--ZD 100815 start--%>
                                        <%--<tr valign="top">
                                            <td align="left" class="tableBody" valign="top" >
                                                <asp:Literal Text="<%$ Resources:WebResources, EditEndpoint_ConfirmPasswor%>" runat="server"></asp:Literal>
                                            </td>
                                        </tr>--%> <%--ZD 100815 Ends--%>
                                    </table>
                                </td>
                                <td>
                                    <table width="100%" cellspacing="0">
                                        <tr valign="top">
                                            <td align="left">
                                                <asp:TextBox CssClass="altText"  ID="txtProfilePassword"  runat="server" TextMode="Password" onblur="PasswordChange(this.value)" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onfocus="javascript:fnTextFocus(this.id,1);"></asp:TextBox>
                                                <input type="hidden" runat="server" id="lbltxtProfilePassword" value="false" />
                                                <input type="hidden" runat="server" id="lbltxtCompare" />
                                                <input type="hidden"  id="hdnPass"  runat="server" />
                                                <asp:RegularExpressionValidator ID="regnumPassword1" runat="server" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters20%>" SetFocusOnError="True" ValidationGroup="Submit" ToolTip="<%$ Resources:WebResources, InvalidCharacters20%>" ControlToValidate="txtProfilePassword" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;|`,\[\]{}\x22;=^:&()~]*$" Display="Dynamic"></asp:RegularExpressionValidator> <%--FB Case 557 Saima--%><%--FB 2319--%> <%--FB 2996--%>
                                                <asp:CompareValidator runat="server" ID="cmpPass1" ControlToCompare="txtProfilePassword2" ControlToValidate="txtProfilePassword" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, ConfirmPassword%>" Display="dynamic"></asp:CompareValidator><%--ZD 100736--%>
                                            </td>
                                        </tr>
                                    <%--<tr valign="top">
                                        <td align="left">
                                            <asp:TextBox CssClass="altText"  ID="txtProfilePassword2" runat="server" TextMode="Password" onblur="PasswordChange(this.value)" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onfocus="javascript:fnTextFocus(this.id,2);" ></asp:TextBox>
                                            <input type="hidden" runat="server" id="lbltxtProfilePassword2" value="false" />
                                            <asp:RegularExpressionValidator ID="regnumPassword2" runat="server" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters20%>" ValidationGroup="Submit" SetFocusOnError="True" ToolTip="<%$ Resources:WebResources, InvalidCharacters20%>" ControlToValidate="txtProfilePassword" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;|`,\[\]{}\x22;=^:&()~]*$" Display="Dynamic"></asp:RegularExpressionValidator> 
                                            <asp:CompareValidator runat="server" ID="cmpPass2" ControlToCompare="txtProfilePassword" ControlToValidate="txtProfilePassword2" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, PasswordNotMatch2%>" Display="dynamic"></asp:CompareValidator>
                                        </td>
                                    </tr>--%>
                                    </table>
                                </td>
                            </tr>
                            <%--ZD 100815 start--%>
                            <tr>
                            <td align="left" class="tableBody">
                            <asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, EndPOintProfile_ProfileType%>" runat="server"></asp:Literal>
                                <span style="color:Red">*</span>
                            </td>
                            <td align="left">
                                <asp:dropdownlist id="lstProfileType" cssclass="altSelectFormat" runat="server" OnSelectedIndexChanged="UpdateProfileType" AutoPostBack="true" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.ProfileType") %>'>
                                    <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:WebResources, Both%>"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="<%$ Resources:WebResources, ConfMCUInfo_MCU%>"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="<%$ Resources:WebResources, ManageConference_pttopt%>"></asp:ListItem>
                                </asp:dropdownlist>
                            </td>
                            <td></td>
                            <td></td>
                           <td align="left" class="tableBody" valign="top" >
                                <asp:Label ID="lblConfirmPassword" Text="<%$ Resources:WebResources, EditEndpoint_ConfirmPasswor%>" runat="server"></asp:Label>
                            </td>
                              <td align="left">
                                    <asp:TextBox CssClass="altText"  ID="txtProfilePassword2" runat="server" TextMode="Password" onblur="PasswordChange(this.value)" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onfocus="javascript:fnTextFocus(this.id,2);" ></asp:TextBox>
                                    <input type="hidden" runat="server" id="lbltxtProfilePassword2" value="false" />
                                    <asp:RegularExpressionValidator ID="regnumPassword2" runat="server" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters20%>" ValidationGroup="Submit" SetFocusOnError="True" ToolTip="<%$ Resources:WebResources, InvalidCharacters20%>" ControlToValidate="txtProfilePassword" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;|`,\[\]{}\x22;=^:&()~]*$" Display="Dynamic"></asp:RegularExpressionValidator> 
                                    <asp:CompareValidator runat="server" ID="cmpPass2" ControlToCompare="txtProfilePassword" ControlToValidate="txtProfilePassword2" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, PasswordNotMatch2%>" Display="dynamic"></asp:CompareValidator>
                              </td>          
                            </tr><%--ZD 100815 End--%>
                            <tr> <%--FB 2400 start--%>
                                <td align="left" class="tableBody"><asp:Label ID="lblTelePresence" Text="<%$ Resources:WebResources, EditEndpoint_TelePresence%>" runat="server"></asp:Label></td>
                                <td align="left">
                                    <asp:CheckBox ID="chkTelepresence" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.isTelePresence").Equals("1") %>' OnClick="javascript:enableSubAddProfile(this)"/>
                                </td>
                                <td align="left" class="tableBody">
                                    <asp:Literal Text="<%$ Resources:WebResources, EditEndpoint_Address%>" runat="server"></asp:Literal> <span style="color:Red">*</span>
                                <td align="left" nowrap="nowrap" width="250px"> <%--ZD 100426--%>
                                    <asp:TextBox CssClass="altText"  ID="txtAddress" runat="server"  value='<%# DataBinder.Eval(Container, "DataItem.Address") %>'></asp:TextBox>  <%-- Text='<%# DataBinder.Eval(Container, "DataItem.Address") %>' SelectedValue='<%# DataBinder.Eval(Container, "DataItem.Address") %>'--%>
                                    <%--FB 1972--%>
                                    <asp:RegularExpressionValidator ID="regAddress" ControlToValidate="txtAddress" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters21%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>?|`\[\]{}\=^$%&~]*$"></asp:RegularExpressionValidator>  <%--FB 2267--%> <%--FB 2594--%>
                                    <%--<asp:RegularExpressionValidator ID="regAddress" ControlToValidate="txtAddress" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" Enabled="false" ErrorMessage="<br>& < > ' + % \ / ( ) ; ? | ^ = ! ` , [ ] { } : # $ @ ~ and &#34; are invalid characters." ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>--%>
                                    <asp:RegularExpressionValidator ID="regMPI" Enabled="false" ControlToValidate="txtAddress" ValidationGroup="Submit" Display="dynamic" runat="server" 
                                        ErrorMessage="<%$ Resources:WebResources, InvalidMPIaddress%>" ValidationExpression="^[0-9A-Za-z]+$"></asp:RegularExpressionValidator>
                                    <asp:Button id="btnProfileAddr" runat="server" Text="<%$ Resources:WebResources, EditEndpoint_btnProfileAddr%>" class="btndisable" Width="60px" Height="20px"/> <%--FB 2664 ZD 100393--%>
									<asp:RequiredFieldValidator ID="reqProfAddress" ControlToValidate="txtAddress" ValidationGroup="Submit" runat="server" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="Dynamic" Enabled="false" ></asp:RequiredFieldValidator>                                    
                                </td>
                                <td align="left" class="tableBody">
                                    <asp:Label ID="lblAddressType" Text="<%$ Resources:WebResources, EditEndpoint_AddressType%>" runat="server"></asp:Label><span style="color:Red">*</span></td>
                                <td align="left">
                                    <asp:DropDownList CssClass="altSelectFormat" ID="lstAddressType" runat="server" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.AddressType") %>' DataTextField="Name" DataValueField="ID" onchange="javascript:ValidateSelection(this);" ></asp:DropDownList> <%--ZD 100393--%>
                                    <asp:RequiredFieldValidator ID="reqAddressType" runat="server" InitialValue="-1" ControlToValidate="lstAddressType" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <%--ZD 100736 START--%>
                            <tr>
                                <td align="left" class="tableBody"><%-- ZD 101344--%>
                                    <asp:Literal Text="<%$ Resources:WebResources, Manufacturer%>" runat="server"></asp:Literal><span style="color:Red">*</span>
                                </td>
                                <td align="left"> <%-- ZD 101344--%>
                                   <asp:DropDownList ID="lstManufacturer" runat="server" OnSelectedIndexChanged="UpdateEndpointModel" AutoPostBack="true" DataTextField="ManufacturerName" DataValueField="ManufacturerID" CssClass="altSelectFormat" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.Manufacturer") %>' ></asp:DropDownList> <%--ZD 100736--%>
                                   <asp:RequiredFieldValidator ID="reqManufacturerType" runat="server" InitialValue="-1" ControlToValidate="lstManufacturer" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>
                                </td>
                                <td colspan="4">
                                </td>
                            </tr>
                            <tr>
                                 <td align="left" class="tableBody">
                                    <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, EditEndpoint_Model%>" runat="server"></asp:Literal>
                                    <br /><br />
                                    <asp:Literal Text="<%$ Resources:WebResources, Resolution%>" runat="server"></asp:Literal><span style="color:Red">*</span><%--ZD 100040--%>
                                    
                                 </td><%--ZD 100736 100040--%>
                                <td align="left">
                                   <asp:DropDownList CssClass="altSelectFormat" ID="lstVideoEquipment" runat="server" DataTextField="EquipmentDisplayName" DataValueField="VideoEquipmentID"  SelectedValue='<%# DataBinder.Eval(Container, "DataItem.VideoEquipment") %>'></asp:DropDownList> <%--ZD 100393--%><asp:RequiredFieldValidator ID="reqVideoEquipment" runat="server" InitialValue="-1" ControlToValidate="lstVideoEquipment" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>                                    
                                     <br /><span style='color: #666666;white-space: nowrap;'><asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ModelText%>" runat="server"></asp:Literal></span><%--ZD 100736--%>
                                    <br />
                                    <asp:DropDownList CssClass="altSelectFormat" ID="lstEptResolution" InitialValue="-1" runat="server" DataTextField="Resolution" DataValueField="RID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.EptResolution") %>' ></asp:DropDownList> <%--ZD 100393--%>
                                    <asp:RequiredFieldValidator ID="reqEptResolution" runat="server" InitialValue="-1" ControlToValidate="lstEptResolution" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidatorRes" runat="server" InitialValue="-1" ControlToValidate="lstEptResolution" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>--%>
                                    
                                    
                                    </td>
                                <%--ZD 100736 END--%>
                                <td align="left" valign="top"></td>
                                <td align="left">
                                    <asp:ListBox runat="server" ID="lstProfileAddress" CssClass="altSelectFormat" Rows="3" SelectionMode="Multiple"  onDblClick="javascript:return AddRemoveList('Rem',this)" onkeydown="if(event.keyCode ==32){javascript:return AddRemoveList('Rem',this)}"></asp:ListBox>  <%--ZD 100420--%>
                                     <%--ZD 100422 Start--%>
                                    <br /><span style='color: #666666;'><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, TelePresenseText%>" runat="server"></asp:Literal></span>
                                    <asp:RequiredFieldValidator ID="reqAddress" ControlToValidate="lstProfileAddress" ValidationGroup="Submit" runat="server" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="Dynamic" Enabled="false" ></asp:RequiredFieldValidator>                                    
                                    <input type="hidden" name="hdnprofileAddresses" id="hdnprofileAddresses" runat="server" value='<%# DataBinder.Eval(Container, "DataItem.MultiCodec") %>' />
                                   
                                </td>
                                <td align="left" class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, EditEndpoint_PreferredBandw%>" runat="server"></asp:Literal><span style="color:Red">*</span></td>
                                <td align="left">
                                    <asp:DropDownList CssClass="altSelectFormat" ID="lstLineRate" runat="server" DataTextField="LineRateName" DataValueField="LineRateID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.LineRate") %>'></asp:DropDownList> <%--ZD 100393--%>
                                    <asp:RequiredFieldValidator ID="reqLineRate" runat="server" InitialValue="-1" ControlToValidate="lstLineRate" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator>                                    
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="tableBody"><asp:Label ID="lblAssignedtoMCU" Text="<%$ Resources:WebResources, EditEndpoint_AssignedtoMCU%>" runat="server"></asp:Label><span style="color:Red">*</span></td>
                                <td align="left" nowrap="nowrap">
                                    <asp:DropDownList CssClass="altSelectFormat" ID="lstBridges" runat="server" DataTextField="MCUNameandtype" DataValueField="BridgeID" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.Bridge") %>'  onchange="javascript:__doPostBack(this.id,'');"  OnSelectedIndexChanged="ChangeMCU"  AutoPostBack="true"></asp:DropDownList> <%--ZD 100393--%>
                                    <asp:RequiredFieldValidator ID="reqBridges" runat="server" InitialValue="-1" Enabled="false" ControlToValidate="lstBridges" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator><%--FB 1901--%>  
                                    <asp:DropDownList CssClass="altSelectFormat" ID="lstBridgeType" runat="server" DataTextField="BridgeType" DataValueField="BridgeID"  SelectedValue='<%# DataBinder.Eval(Container, "DataItem.Bridge") %>' Visible="false" ></asp:DropDownList> <%--ZD 100619--%>
                                       <%--<asp:Literal  Text="<%$ Resources:WebResources, LeastCostRoute%>" runat="server"></asp:Literal>--%>
                                       <asp:CheckBox ID="ChkLCR" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.LCR").Equals("1") %>' Visible="false" />   <%-- ZD 100040 commented in Phase I --%>      <%--Checked='<%# DataBinder.Eval(Container, "DataItem.EncryptionPreferred").Equals("1") %>'  --%>             
                                 </td>
                                 <td align="left" class="tableBody"><asp:Label ID="lblPreferredDialOption" Text="<%$ Resources:WebResources, EditEndpoint_PreferredDiali%>" runat="server"></asp:Label><span style="color:Red">*</span></td>
                                <td align="left">
                                    <asp:DropDownList CssClass="altSelectFormat" ID="lstConnectionType" runat="server" DataTextField="Name" DataValueField="ID"   OnSelectedIndexChanged="ChangeMCU"  AutoPostBack="true" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.ConnectionType") %>' onchange="javascript:ValidateSelection(this);" > <%--ZD 100393--%>
                                    </asp:DropDownList><%--Fogbugz case 427--%>
                                    <asp:RequiredFieldValidator ID="reqPrefDial" runat="server" InitialValue="-1" Enabled="false" ControlToValidate="lstConnectionType" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic"></asp:RequiredFieldValidator><%--FB 2093--%> 
                                </td>
                                <td align="left" class="tableBody"><asp:Label ID="lblDefaultProtocol" Text="<%$ Resources:WebResources, EditEndpoint_DefaultProtoco%>" runat="server"></asp:Label></td>
                                <td align="left">
                                    <asp:DropDownList CssClass="altSelectFormat" ID="lstVideoProtocol" runat="server" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.DefaultProtocol") %>' DataTextField="Name" DataValueField="ID" onchange="javascript:ValidateSelection(this);"></asp:DropDownList> <%--ZD 100393--%>
                                </td>
                            </tr>
                            <%--ZD 104821 Starts--%>
                            <tr id="trsyslocs" runat="server">
                                <td align="left" class="tableBody"><asp:Label ID="SysLocation"  Text="<%$ Resources:WebResources, SystemLocation%>" runat="server"></asp:Label></td>
                                <td align="left" nowrap="nowrap">
                                    <asp:DropDownList ID="lstSystemLocation" CssClass="altSelectFormat" runat="server" DataValueField="SysLocId" DataTextField="Name" >
                                    </asp:DropDownList>
                                    <asp:TextBox ID="txtSysLoc" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container, "DataItem.SysLocation") %>'></asp:TextBox>
                                 </td>
                            </tr>
                            <%--ZD 104821 Ends--%>
                            <tr>
                                <td align="left" class="tableBody"><asp:Label  ID="lblAssociateMCUAddress" Text="<%$ Resources:WebResources, EditEndpoint_Associatewith%>" runat="server"></asp:Label></td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText"  ID="txtMCUAddress" runat="server" TextMode="SingleLine" Text='<%# DataBinder.Eval(Container, "DataItem.MCUAddress") %>'></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegMCUAddress" ControlToValidate="txtMCUAddress" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td>
                                <td align="left" class="tableBody"><asp:Label ID="lblMCUAddressType" Text="<%$ Resources:WebResources, EditEndpoint_MCUAddressTyp%>" runat="server"></asp:Label></td>
                                <td align="left">
                                    <asp:DropDownList CssClass="altSelectFormat" ID="lstMCUAddressType" runat="server" SelectedValue='<%# DataBinder.Eval(Container, "DataItem.MCUAddressType") %>' DataTextField="Name" DataValueField="ID" onchange="javascript:ValidateSelection(this);" ></asp:DropDownList> <%--ZD 100393--%>
                                </td>
                                <td align="left" class="tableBody"><asp:Label ID="lblWebAccessURL" Text="<%$ Resources:WebResources, EditEndpoint_WebAccessURL%>" runat="server"></asp:Label></td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText"  ID="txtURL" runat="server" TextMode="SingleLine" Text='<%# DataBinder.Eval(Container, "DataItem.URL") %>'></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegURL" ControlToValidate="txtURL" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters3%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^@#$%&()'~]*$"></asp:RegularExpressionValidator>    
                                </td>
                            </tr>
                            
                             <%-- Code Added For FB 1422- Start--%>                            
                            <tr>
                            <td align="left" class="tableBody"><asp:Label  ID="lbliCalInvite" Text="<%$ Resources:WebResources, EditEndpoint_iCalInvite%>" runat="server"></asp:Label></td>
                                <td align="left">
                                    <asp:CheckBox ID="chkIsCalderInvite" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.IsCalendarInvite").Equals("1") %>' />
                                </td>
                               <td align="left" class="tableBody"><asp:Label ID="lblEncryptionPreferred" Text="<%$ Resources:WebResources, EditEndpoint_EncryptionPref%>" runat="server"></asp:Label></td>
                                <td align="left">
                                    <asp:CheckBox ID="chkEncryptionPreferred" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.EncryptionPreferred").Equals("1") %>'/>
                                </td>
                                <td align="left" class="tableBody"><asp:Label ID="lblTelnetAPIEnabled"  Text="<%$ Resources:WebResources, EditEndpoint_TelnetAPIEnab%>" runat="server"></asp:Label></td>
                                <td align="left">
                                    <asp:CheckBox ID="chkP2PSupport" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.TelnetAPI").Equals("1") %>'/>
                                </td>
                            </tr>
                            <%-- Code Added For FB 1422- End--%> 
                            <tr>
                                <td align="left" class="tableBody"><asp:Label ID="lblEmailID" Text="<%$ Resources:WebResources, EditEndpoint_EmailID%>" runat="server"></asp:Label></td> <%--ICAL Fix--%>
                                <td align="left">
                                    <asp:TextBox CssClass="altText"  ID="txtExchangeID" runat="server" Width="150px" TextMode="SingleLine" Text='<%# DataBinder.Eval(Container, "DataItem.ExchangeID") %>'></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegExchangeID" ControlToValidate="txtExchangeID" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters22%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^#$%&()'~]*$"></asp:RegularExpressionValidator>
                                </td>
                                
                                <%--API Port Starts--%>
                                <td align="left" class="tableBody"><asp:Label ID="lblAPIPort" Text="<%$ Resources:WebResources, EditEndpoint_APIPort%>" runat="server"></asp:Label></td>
                                <td align="left"> <!-- FB 2050 -->
                                    <asp:TextBox   ID="txtApiport" runat="server" MaxLength="5"  Text='<%# DataBinder.Eval(Container, "DataItem.apiPortno") %>' CssClass="altText"></asp:TextBox>                                
                                    <asp:RegularExpressionValidator ID="RegApiport" ControlToValidate="txtApiport" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly%>" ValidationExpression="^\d{1,5}$"></asp:RegularExpressionValidator>
                                </td>
                               <%--API Port Ends--%>
                               <td align="left" class="tableBody"><asp:Label ID="lblSSH"  Text="<%$ Resources:WebResources, EditEndpoint_SSHSupport%>" runat="server"></asp:Label></td>
                               <td align="left">
                                    <asp:CheckBox ID="chkSSHSupport" runat="server" Checked='<%# DataBinder.Eval(Container, "DataItem.SSHSupport").Equals("1") %>' />
                                </td>                               
                            </tr>
                            <%--ZD 100132 START--%>
                            <tr>
                            <td align="left" class="tableBody"><asp:Label ID="lblRearSecurityCameraAddress" Text="<%$ Resources:WebResources, EditEndpoint_RearSecurityCa%>" runat="server"></asp:Label></td>
                                <td align="left">
                                    <asp:TextBox ID="txtRearSecCamAdd" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RearSecCameraAddress") %>' CssClass="altText" ValidationGroup="Submit"></asp:TextBox><br />
                                    <asp:RegularExpressionValidator ID="regRearSecCamAdd" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtRearSecCamAdd" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="<%$ Resources:WebResources, InvalidIPAddress%>" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                                </td>
                            <td align="left" class="tableBody"><asp:Literal Text="<%$ Resources:WebResources, EditEndpoint_LocatedOutside%>" runat="server"></asp:Literal></td>
                                <td align="left">
                                    <asp:CheckBox ID="chkIsOutside" runat="server" onclick="javascript:fnOpengatekeeper(this)" Checked='<%# (DataBinder.Eval(Container, "DataItem.IsOutside").Equals("1")) %>' /><%--ZEN 100132--%>
                                </td>
                                <td align="left" id="tdGateKeeeperAddress" runat="server">
                                   <asp:Label id="LblGateKeeeperAddress" runat="server" text="<%$ Resources:WebResources, EditEndpoint_LblGateKeeeperAddress%>"></asp:Label>
                                   </td>
                                <td align="left">
                                    <asp:TextBox CssClass="altText"  ID="txtGateKeeeperAddress" runat="server"  Text='<%# DataBinder.Eval(Container, "DataItem.GateKeeeperAddress") %>' Width="150px"></asp:TextBox>
                                    </td>
                            </tr>
                            <%--ZEN 100132 End--%>
                            <tr>
                                <%--FB 2595 Start --%>
                                <td align="left" class="tableBody">
                                <asp:Label id="lblSecured" runat="server" text="<%$ Resources:WebResources, EditEndpoint_lblSecured%>"></asp:Label>
                                </td>
                                    <td align="left"> 
                                    <asp:CheckBox ID="chkSecured" runat="server" onclick="javascript:fnSecuredDetails(this)" Checked='<%# DataBinder.Eval(Container, "DataItem.Secured").Equals("1") %>' />
                                </td>
                                <td align="left" class="tableBody" id="tdNetworkURL" runat="server">
                                <asp:Label id="lblNetworkURL" runat="server" text="<%$ Resources:WebResources, EditEndpoint_lblNetworkURL%>"></asp:Label>
                                </td>
                                <td align="left" id="tdTxtNetworkURL" runat="server"> 
                                     <asp:TextBox CssClass="altText"  ID="txtNetworkURL" runat="server" TextMode="SingleLine" Text='<%# DataBinder.Eval(Container, "DataItem.NetworkURL") %>'></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                            <td align="left" class="tableBody" id="tdSecuredPort" runat="server">
                             <asp:Label id="lblSecuredPort" runat="server" text="<%$ Resources:WebResources, EditEndpoint_lblSecuredPort%>"></asp:Label>
                                </td>
                                <td align="left" id="tdTxtSecuredPort" runat="server">
                                    <asp:TextBox ID="txtSecuredport" runat="server" MaxLength="5"  Text='<%# DataBinder.Eval(Container, "DataItem.Securedport") %>' CssClass="altText"></asp:TextBox>                                
                                    <asp:RegularExpressionValidator ID="RegSecuredport" ControlToValidate="txtSecuredport" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly%>" ValidationExpression="^\d{1,5}$"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <%--FB 2595 End --%>
                        </table>
                        </ItemTemplate>
                        <FooterTemplate>
                        </FooterTemplate>
                        <FooterStyle BackColor="beige" />
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Default%>" ItemStyle-VerticalAlign="Top">
                    <HeaderStyle CssClass="tableHeader" />
                        <ItemTemplate>
                            <asp:CheckBox ID="rdDefault" runat="server" onclick="javascript:SelectOneDefault(this)" GroupName="Default" Checked='<%# DataBinder.Eval(Container, "DataItem.DefaultProfile").Equals("1") %>' /></td> <%--ZD 100815--%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Delete%>" ItemStyle-VerticalAlign="Top">
                    <HeaderStyle CssClass="tableHeader" />
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDelete" runat="server" Checked="false" onclick="javascript:CheckDefault(this)" />
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
                <SelectedItemStyle BackColor="Beige" />
             </asp:DataGrid>
         <tr>
            <td align="center">
				<%--ZD 100420--%>
                <%--<asp:Button ID="btnCancel" runat="server" CssClass="altMedium0BlueButtonFormat" Text="Cancel" OnClick="CancelEndpoint" OnClientClick="DataLoading(1)" />--%>
                <button ID="btnCancel" type="button" runat="server" class="altMedium0BlueButtonFormat" onserverclick="CancelEndpoint" onclick="DataLoading(1);" >
				<asp:Literal Text="<%$ Resources:WebResources, EditEndpoint_btnCancel%>" runat="server"></asp:Literal></button> 
                <%--<asp:Button ID="btnAddNewProfile" runat="server"  Text="Submit/Add New Profile" OnClick="AddNewProfile" CausesValidation="true" ValidationGroup="Submit"  Width="205px" Height="26px" />--%>
                <button ID="btnAddNewProfile" type="button" runat="server" onserverclick="AddNewProfile" CausesValidation="true" ValidationGroup="Submit" style="Width:255px;Height:26px" >
				<asp:Literal Text="<%$ Resources:WebResources, EditEndpoint_btnAddNewProfile%>" runat="server"></asp:Literal></button><%--ZD 100381--%>
                <asp:Button ID="btnSubmit" runat="server"  Text="<%$ Resources:WebResources, EditEndpoint_btnSubmit%>" OnClick="SubmitEndpoint" OnClientClick="javascript:return IsMarkedForDeletion();" Width="100pt" ValidationGroup="Submit"/>                
				<%--ZD 100420--%>
                <asp:CustomValidator ID="cvSubmit" ValidationGroup="Submit" OnServerValidate="ValidateInput" SetFocusOnError="true" runat="server" ErrorMessage="<%$ Resources:WebResources, ResponseConference_InvalidIPISDN%>" Display="dynamic"></asp:CustomValidator>
            </td>
         </tr>

        </table>
</center>
                <input type="hidden" id="helpPage" value="29">
                <input type="hidden" id="isMarkedDeleted" value="0" runat="server" />
                <input type="hidden" id="hdnTmpPass" value="" runat="server"/> <%--ZD 100736--%>
    
    <script language="javascript" type="text/javascript">

        //FB 2400 start
        function enableSubAddProfile(obj)
         {
            var datagridID = obj.id.replace(obj.id.split("_", 3)[2], "");
            if (document.getElementById(obj.id).checked) {

                 document.getElementById(datagridID + "btnProfileAddr").disabled = false;
                 document.getElementById(datagridID + "btnProfileAddr").className = "altShortBlueButtonFormat"; //FB 2664
                 document.getElementById(datagridID + "lstProfileAddress").disabled = false;
                 //document.getElementById("btnAddNewProfile").disabled = true; //FB 2602
                 AddRemoveList('add', obj)

                 if (document.getElementById(datagridID + "lstProfileAddress").options.length > 0) // ZD Latest
                     ValidatorEnable(document.getElementById(datagridID + "reqProfAddress"), false);
             }
             else {
                 ValidatorEnable(document.getElementById(datagridID + "reqProfAddress"), true); // ZD Latest

                 document.getElementById(datagridID + "btnProfileAddr").disabled = true;
                 document.getElementById(datagridID + "btnProfileAddr").className = "btndisable"; //FB 2664
                 document.getElementById(datagridID + "lstProfileAddress").disabled = true;
                 if('<%=Session["admin"]%>' != "3") //FB 2670
                    document.getElementById("btnAddNewProfile").disabled = false;
                 
                 var lstBox = document.getElementById(datagridID + "lstProfileAddress");
                 if (lstBox.options.length > 0)
                 {
                     document.getElementById(datagridID + "txtAddress").value = lstBox.options[0].text
                     document.getElementById(datagridID + "hdnprofileAddresses").value = "";
                 }

                 for (i = lstBox.options.length - 1; i >= 0; i--)
                     lstBox.remove(i);
                 
             }

            return false;
        }
    
        function AddRemoveList(opr,obj)
        {
            if (obj.id == null)
                return false;
            
            var datagridID = obj.id.replace(obj.id.split("_",3)[2], "");
            var lstBox = document.getElementById(datagridID + "lstProfileAddress");
            var txtUsrInput = document.getElementById(datagridID + "txtAddress");
            var hdnprofileAddresses = document.getElementById(datagridID + "hdnprofileAddresses");
            ValidatorEnable(document.getElementById(datagridID + "reqAddress"), false);
            
            //ZD 100381
            var IE11 = false;
            if (navigator.userAgent.indexOf('Trident/7.0') > -1) 
                if (lstBox.options.length == 1)
                    IE11 = true;
                      
            if (opr == "Rem") {
                var i;
                for (i = lstBox.options.length - 1; i >= 0; i--) {
                    if (lstBox.options[i].selected || IE11) {

                        if (lstBox.options.length > 1)
                        {
                            if (i == lstBox.options.length - 1)
                                lstBox.options[i].text = "~" + lstBox.options[i].text;
                            else if (i == 0)
                                lstBox.options[i].text = lstBox.options[i].text + "~";
                        }                        
                        hdnprofileAddresses.value = hdnprofileAddresses.value.replace(lstBox.options[i].text, "").replace(/��/i, "~")
                        lstBox.remove(i);
                    }
                }
            }
            else if (opr == "add") {
            if (txtUsrInput.value.replace(/\s/g, "") == "") //trim the textbox
                return false;
            
                if (lstBox.options.length >= 8) {
                    document.getElementById("errLabel").innerHTML = "Maximum 8 addresses";
                    document.getElementById("errLabel").focus();
                    return false;
                }
                else {
                    for (i = lstBox.options.length - 1; i >= 0; i--)
                    {
                        if (lstBox.options[i].text.replace(/\s/g, "") == txtUsrInput.value.replace(/\s/g, ""))
                        {
                            document.getElementById("errLabel").innerHTML = "Already Added address";
                            return false;
                        }
                    }
                }

                if (lstBox.options.length > 0)
                    hdnprofileAddresses.value = hdnprofileAddresses.value + "~";
                
                var option = document.createElement("Option");
                option.text = txtUsrInput.value;
                option.title = txtUsrInput.value;
                lstBox.add(option);
                              
                hdnprofileAddresses.value = hdnprofileAddresses.value + txtUsrInput.value;

                txtUsrInput.value = "";
                txtUsrInput.focus();


            }

            if (lstBox.options.length > 0) // ZD Latest
                ValidatorEnable(document.getElementById(datagridID + "reqProfAddress"), false);
            else
                ValidatorEnable(document.getElementById(datagridID + "reqProfAddress"), true);


            return false;
        }

        // ZD 100263
        function fnSetPasswordStatus() {

            var tmpPassword = document.getElementById("hdnTmpPass"); // ZD 100736
            
            if ('<%=Session["EndpointID"]%>' == 'new') {
                document.getElementById("dgProfiles_ctl02_txtProfilePassword").style.backgroundImage = "";
                document.getElementById("dgProfiles_ctl02_txtProfilePassword2").style.backgroundImage = "";

                document.getElementById("dgProfiles_ctl02_txtProfilePassword").value = tmpPassword.value; // ZD 100736
                document.getElementById("dgProfiles_ctl02_txtProfilePassword2").value = tmpPassword.value; // ZD 100736

            }
            if ('<%=Session["profCnt"]%>' != '') {
                document.getElementById("dgProfiles_ctl0" + '<%=Session["profCnt"]%>' + "_txtProfilePassword").style.backgroundImage = "";
                document.getElementById("dgProfiles_ctl0" + '<%=Session["profCnt"]%>' + "_txtProfilePassword2").style.backgroundImage = "";

                document.getElementById("dgProfiles_ctl0" + '<%=Session["profCnt"]%>' + "_txtProfilePassword").value = tmpPassword.value; // ZD 100736
                document.getElementById("dgProfiles_ctl0" + '<%=Session["profCnt"]%>' + "_txtProfilePassword2").value = tmpPassword.value; // ZD 100736

            }
        }
        //setTimeout("fnSetPasswordStatus()", 100);
        document.getElementById('txtEndpointName').setAttribute("onfocus", "setTimeout('window.scrollTo(0,0);', 1000)");

        if (document.getElementById('btnAddNewProfile') != null)
            document.getElementById('btnAddNewProfile').setAttribute("onblur", "document.getElementById('btnSubmit').focus(); document.getElementById('btnSubmit').setAttribute('onfocus', '');");               

        document.onkeydown = function(evt) {
            evt = evt || window.event;
            var keyCode = evt.keyCode;
            if (keyCode == 8) {
                if (document.getElementById("btnCancel") != null) { // backspace
                    var str = document.activeElement.type;
                    if (!(str == "text" || str == "textarea" || str == "password")) {
                        document.getElementById("btnCancel").click();
                        return false;
                    }
                }
                if (document.getElementById("btnGoBack") != null) { // backspace
                    var str = document.activeElement.type;
                    if (!(str == "text" || str == "textarea" || str == "password")) {
                        document.getElementById("btnGoBack").click();
                        return false;
                    }
                }
            }
            fnOnKeyDown(evt);
        };
        
        
        
    </script>
    
    <script language="javascript" type="text/javascript">
    //ZD 100736 START
        function PasswordChange(par) { 
                document.getElementById("hdnTmpPass").value = par;
        }
        //ZD 100736 END
        
    </script>     
    
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->