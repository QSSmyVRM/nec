﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
#region References
using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Net;
using System.IO;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using System.Data;
using System.Xml;
using System.Collections;
using System.ComponentModel;
using System.Web;
using myVRM.DataLayer;
using System.Collections.Generic;
using System.ServiceProcess;

#endregion

namespace myVRMLDAPService
{
    public partial class myVRMLDAP : ServiceBase
    {
        string dirPth = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        ASPIL.VRMServer myvrmCom = new ASPIL.VRMServer();
        System.Timers.Timer timerSync = new System.Timers.Timer();
        NS_CONFIG.Config config = null;
        NS_MESSENGER.ConfigParams configParams = null;
        string errMsg = null;
        NS_LOGGER.Log log = null;
        bool ret = false;
        string MyVRMServer_ConfigPath = "";
        string COM_ConfigPath = "";
        string RTC_ConfigPath = "";
        string configPath = "";
        static System.Timers.Timer timerAutoPurgelogs = new System.Timers.Timer();//ZD 104846 start
        static System.Globalization.CultureInfo globalCultureInfo = new System.Globalization.CultureInfo("en-US", true);
        static int purgeDuration = 0;//ZD 104846 end

        public myVRMLDAP()
        {
            InitializeComponent();

            MyVRMServer_ConfigPath = dirPth + "\\VRMSchemas\\";
            COM_ConfigPath = dirPth + "\\VRMSchemas\\COMConfig.xml";
            RTC_ConfigPath = dirPth + "\\VRMSchemas\\VRMRTCConfig.xml";
            configPath = dirPth + "\\VRMMaintServiceConfig.xml";
        }

        protected override void OnStart(string[] args)
        {
            double PurgeLogsInterval = 24 * 60 * 60 * 1000; //ZD 104846 //24 Hours
            
            try
            {
                config = new NS_CONFIG.Config();
                configParams = new NS_MESSENGER.ConfigParams();
                ret = config.Initialize(configPath, ref configParams, ref errMsg, MyVRMServer_ConfigPath, RTC_ConfigPath);
                log = new NS_LOGGER.Log(configParams);


                log.Trace("Into The service started");
                log.Trace("Various Configs COM:" + COM_ConfigPath + " RTC:" + RTC_ConfigPath + " ASPIL:" + MyVRMServer_ConfigPath);
                log.Trace("Site URL: " + configParams.siteUrl);
                log.Trace("ActivationTimer:" + configParams.activationTimer);

                timerSync.Elapsed += new System.Timers.ElapsedEventHandler(timerSync_Elapsed);
                timerSync.Interval = 30 * 1000;
                timerSync.Enabled = true;
                timerSync.AutoReset = true;
                timerSync.Start();


                //ZD 104846 start
                timerAutoPurgelogs.Elapsed += new System.Timers.ElapsedEventHandler(timerAutoPurgelogs_Elapsed);
                timerAutoPurgelogs.Interval = PurgeLogsInterval;
                timerAutoPurgelogs.Enabled = true;
                timerAutoPurgelogs.Start();
                //ZD 104846 End
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }

        void timerSync_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            string searchConfInXML = "";
            string searchConfOutXML = "";
            try
            {
                timerSync.Enabled = false;
                timerSync.AutoReset = false;
                timerSync.Stop();

                myvrmCom = new ASPIL.VRMServer();
                searchConfInXML = "<LDAP><UserID>11</UserID></LDAP>";
                searchConfOutXML = myvrmCom.Operations(MyVRMServer_ConfigPath, "SyncWithLdap", searchConfInXML);

                timerSync.Enabled = true;
                timerSync.AutoReset = true;
                timerSync.Start();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + ex.ToString());
            }
        }

        protected override void OnStop()
        {
            timerSync.Enabled = false;
            timerSync.AutoReset = false;
            timerSync.Stop();
        }

        //ZD 104846 start
        #region timerAutoPurgelogs_Elapsed
        /// <summary>
        /// timerAutoPurgelogs_Elapsed
        /// </summary>
        void timerAutoPurgelogs_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = globalCultureInfo;
                log.Trace("Purge Logs:" + DateTime.Now.ToString("F"));
                timerAutoPurgelogs.AutoReset = false;
                GetSitePurgeLogDuartion();
                PurgeLogs();
                System.Threading.Thread.Sleep(5000);
                timerAutoPurgelogs.AutoReset = true; ;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }

        #endregion

        #region GetSitePurgeLogDuartion
        /// <summary>
        /// GetSitePurgeLogDuartion()
        /// </summary>
        private void GetSitePurgeLogDuartion()
        {
            XmlDocument xmldoc = new XmlDocument();
            string stmt = "", schemapath = "";
            try
            {
                log.Trace("Into GetSitePurgeLogDuartion... " + DateTime.Now.ToLocalTime());
                schemapath = "C:\\VRMSchemas_v1.8.3\\";
                ns_SqlHelper.SqlHelper sqlCon = new ns_SqlHelper.SqlHelper(schemapath);
                sqlCon.OpenConnection();
                stmt = "select AutoPurgeLogDuration from Sys_Settings_D";
                System.Data.DataSet ds = sqlCon.ExecuteDataSet(stmt);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        int.TryParse(ds.Tables[0].Rows[0]["AutoPurgeLogDuration"].ToString(), out purgeDuration);
                    }
                }
                sqlCon.CloseConnection();
            }

            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion

        #region PurgeLogs
        /// <summary>
        /// PurgeLogs
        /// </summary>
        private void PurgeLogs()
        {
            string[] Files = null;
            try
            {
                if (Directory.Exists(MyVRMServer_ConfigPath + "\\MaintenanceLogs"))
                {
                    Files = Directory.GetFiles(MyVRMServer_ConfigPath + "\\MaintenanceLogs");
                    for (int i = 0; i < Files.Length; i++)
                    {
                        FileInfo fi = new FileInfo(Files[i]);
                        if (DateTime.UtcNow - fi.CreationTimeUtc > TimeSpan.FromDays(purgeDuration))
                            fi.Delete();

                    }
                }
                if (Directory.Exists(MyVRMServer_ConfigPath + "\\LDAPLogs"))
                {
                    Files = Directory.GetFiles(MyVRMServer_ConfigPath + "\\LDAPLogs");
                    for (int i = 0; i < Files.Length; i++)
                    {
                        FileInfo fi = new FileInfo(Files[i]);
                        if (DateTime.UtcNow - fi.CreationTimeUtc > TimeSpan.FromDays(purgeDuration))
                            fi.Delete();
                    }
                }

                log.Trace("Deleted Reminder Service  Logs");
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion

        //ZD 104846 End
    }
}
